package ee.buslines;

import java.util.Collection;

/**
*	Bussiliini objekt
*/
public class BusLine {
	
	/**
	*	Bussiliini unikaalne id
	*/
	private Integer id = 0;
	
	/**
	*	Bussiliini number
	*/
	private Integer number = 0;
	
	/**
	*	Bussiliini kestvus
	*/
	private double duration = 0.0;
	
	/**
	*	Collection bussiliinis olevate peatustega
	*/
	private Collection<BusStop> stops;
	
	/**
	*	Meetod bussiliini id saamiseks
	*	@return Bussiliini id
	*/
	public Integer getId() {
		return id;
	}
	
	/**
	*	Meetod bussiliini id sättimiseks
	*	@param id Uus id
	*/
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	*	Meetod bussiliini numbri saamiseks
	*	@return Bussiliini number
	*/
	public Integer getNumber() {
		return number;
	}
	
	/**
	*	Meetod bussiliini numbri sättimiseks
	*	@param number Uus number
	*/
	public void setNumber(Integer number) {
		this.number = number;
	}
	
	/**
	*	Meetod bussiliini kestvuse saamiseks
	*	@return Kestvus
	*/
	public double getDuration() {
		return duration;
	}
	
	/**
	*	Meetod bussiliini kestvuse sättimiseks
	*	@param duration Uus kestvus
	*/
	public void setDuration(double duration) {
		this.duration = duration;
	}
	
	/**
	*	Meetod bussiliini peatuste saamiseks
	*	@return Collection Bussiliini peatustega
	*/
	public Collection<BusStop> getStops() {
		return stops;
	}
	
	/**
	*	Meetod bussiliini peatuste sättimiseks
	*	@param stops Collection bussiliini peatustega
	*/
	public void setStops(Collection<BusStop> stops) {
		this.stops = stops;
	}

	
	
}
