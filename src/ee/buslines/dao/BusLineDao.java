package ee.buslines.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import ee.buslines.BusLine;

/**
*	Bussiliini andmejuurdepääsu objekt
*/
public class BusLineDao extends HibernateDaoSupport {

	
	/**
	*	Meetod kõikide bussiliinide saamiseks andmebaasist
	*	@return List kõikide bussiliinidega
	*/
	@SuppressWarnings("unchecked")
	public List<BusLine> getBusLines() throws DataAccessException {
		return getHibernateTemplate().find("from BusLine line order by line.number");
	}
	
	/**
	*	Meetod bussiliini saamiseks andmebaasist id järgi
	*	@param id Id
	*	@return Bussiliin id'ga id
	*	@throws DataAccessException
	*/
	public BusLine loadBusLine(int id) throws DataAccessException{
		return (BusLine)getHibernateTemplate().load(BusLine.class, new Integer(id));

	}
	
	/**
	*	Meetod bussiliini eemaldamiseks andmebaasist id järgi
	*	@param id Id
	*	@throws DataAccessException
	*/
	public void removeBusLine(int id) throws DataAccessException{
		BusLine busLine = loadBusLine(id);
		getHibernateTemplate().delete(busLine);

	}
	
	/**
	*	Meetod bussiliini salvestamiseks andmebaasi
	*	@param busLine Bussiliin
	*/
	public void saveBusLine(BusLine busLine) throws DataAccessException{
		getHibernateTemplate().saveOrUpdate(busLine);
	}
	
	
}
