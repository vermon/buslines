package ee.buslines.mvc.controller;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import ee.buslines.BusLine;
import ee.buslines.manager.Manager;

/**
*	Kontroller bussiliini lisamisele
*/
public class AddBusLineController extends SimpleFormController {

	/**
	*	Logija tegevuste logimiseks
	*/
	protected final Logger logger = Logger.getLogger(getClass());
	
	/**
	*	Bussiliinide ja peatuste manager
	*/
	private Manager manager;
	
	/**
	*	Meetod vormi saatmise korral bussiliini objekti loomiseks ja uue vaate tagastamiseks
	*	@param command Uus bussiliin
	*	@return Järgmine vaade
	*	@throws ServletException
	*/
    public ModelAndView onSubmit(Object command)
            throws ServletException {
    	
        	BusLine busLine = ((BusLine)command);
        	manager.saveBusLine(busLine);
        	logger.info("Bussiliin nr "+busLine.getNumber()+" salvestatud");
 
  

        return new ModelAndView(new RedirectView(getSuccessView()));
    }
    
	
	/**
	*	Meetod vormi algsete väärtuste(algse bussiliini) sättimiseks
	*	@param request Liides päringust parameetrite saamiseks
	*	@return Algne bussiliin
	*	@throws ServletException
	*/
    protected Object formBackingObject(HttpServletRequest request) throws ServletException {

        	BusLine busLine = new BusLine();
            return busLine;
    }
    

	/**
	*	Meetod manageri saamiseks
	*	@return Manager
	*/
	public Manager getManager() {
		return manager;
	}
	
	/**
	*	Meetod manageri sättimiseks
	*	@param manager Manager
	*/
	public void setManager(Manager manager) {
		this.manager = manager;
	}


}
