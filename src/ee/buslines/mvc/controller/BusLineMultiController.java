package ee.buslines.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import ee.buslines.BusLine;
import ee.buslines.BusStop;
import ee.buslines.manager.Manager;

/**
 * Multikontroller bussiliinide ja -peatuste eemaldamisele ja kuvamisele, ning
 * bussiliinile peatuste lisamisele
 */
public class BusLineMultiController extends MultiActionController {

	/**
	 * Logija tegevuste logimiseks
	 */
	protected final Logger log = Logger.getLogger(getClass());

	/**
	 * Bussiliinide ja peatuste manager
	 */
	private Manager manager;

	/**
	 * Tõeväärtus, mis näitab kas meetodit show on kutsutud otse või mõne selle
	 * klassi teise meetodi kaudu.
	 * 
	 * @see #show(HttpServletRequest, HttpServletResponse)
	 */
	Boolean direct = true;;

	/**
	 * Meetod bussiliini eemaldamiseks ja bussiliinide kuvamise vaate
	 * tagastamiseks
	 * 
	 * @param req
	 *            Liides päringust parameetrite saamiseks
	 * @param res
	 *            Liides päringu edasi saatmiseks
	 * @return Bussiliinide ja -peatuste kuvamise vaade
	 * @see #show(HttpServletRequest, HttpServletResponse)
	 */
	public ModelAndView deleteLine(HttpServletRequest req,
			HttpServletResponse res) {
		direct = false;
		int id = Integer.parseInt(req.getParameter("line"));
		Integer number = manager.getBusLineById(id).getNumber();
		log.info("Bussiliini id on " + id);
		manager.removeBusLine(id);
		log.info("Bussiliin numbriga " + number + " eemaldatud");
		ModelAndView mav = show(req, res);
		return mav;
	}

	/**
	 * Meetod bussipeatuse eemaldamiseks ja bussiliinide kuvamise vaate
	 * tagastamiseks
	 * 
	 * @param req
	 *            Liides päringust parameetrite saamiseks
	 * @param res
	 *            Liides päringu edasi saatmiseks
	 * @return Bussiliinide ja -peatuste kuvamise vaade
	 * @see #show(HttpServletRequest, HttpServletResponse)
	 */
	public ModelAndView deleteStop(HttpServletRequest req,
			HttpServletResponse res) {
		direct = false;
		int id = Integer.parseInt(req.getParameter("stop"));
		String name = manager.getBusStopById(id).getName();
		log.info("Bussipeatuse Id on " + id);
		manager.removeBusStop(id);
		log.info("Peatus nimega " + name + " eemaldatud");
		ModelAndView mav = show(req, res);
		return mav;
	}

	/**
	 * Meetod bussipeatuse liinist eemaldamiseks ja bussiliinide kuvamise vaate
	 * tagastamiseks
	 * 
	 * @param req
	 *            Liides päringust parameetrite saamiseks
	 * @param res
	 *            Liides päringu edasi saatmiseks
	 * @return Bussiliinide ja -peatuste kuvamise vaade
	 * @see #show(HttpServletRequest, HttpServletResponse)
	 */
	public ModelAndView removeStopFromLine(HttpServletRequest req,
			HttpServletResponse res) {
		direct = false;
		int lineId = Integer.parseInt(req.getParameter("line"));
		int stopId = Integer.parseInt(req.getParameter("stop"));
		BusLine busLine = manager.getBusLineById(lineId);
		HashSet<BusStop> list = new HashSet<BusStop>();
		for (BusStop stop : busLine.getStops()) {
			if (stop.getId() != stopId) {
				list.add(stop);
			}
		}
		busLine.setStops(list);
		manager.saveBusLine(busLine);
		log.info("Busiliinist nr " + busLine.getNumber()
				+ " on eemaldatud peatus "
				+ manager.getBusStopById(stopId).getName());
		ModelAndView mav = show(req, res);
		return mav;
	}

	/**
	 * Meetod bussipeatuse liini lisamiseks ja bussiliinide kuvamise vaate
	 * tagastamiseks
	 * 
	 * @param req
	 *            Liides päringust parameetrite saamiseks
	 * @param res
	 *            Liides päringu edasi saatmiseks
	 * @return Bussiliinide ja -peatuste kuvamise vaade
	 * @see #show(HttpServletRequest, HttpServletResponse)
	 */
	public ModelAndView addStopToLine(HttpServletRequest req,
			HttpServletResponse res) {
		direct = false;
		int lineId = Integer.parseInt(req.getParameter("line"));
		int stopId = Integer.parseInt(req.getParameter("stop"));
		BusLine busLine = manager.getBusLineById(lineId);
		BusStop busStop = manager.getBusStopById(stopId);
		if (!busLine.getStops().contains(busStop)) {
			busLine.getStops().add(busStop);
			manager.saveBusLine(busLine);
		}
		log.info("Busiliini nr " + busLine.getNumber() + " on lisatud peatus "
				+ busStop.getName());
		ModelAndView mav = show(req, res);
		return mav;
	}

	/**
	 * Meetod bussiliinide ja -peatuste vaate tagastamiseks
	 * 
	 * @param req
	 *            Liides päringust parameetrite saamiseks
	 * @param res
	 *            Liides päringu edasi saatmiseks
	 * @return Vaade bussiliinide ja -peatustega
	 */
	public ModelAndView show(HttpServletRequest req, HttpServletResponse res) {
		ModelAndView mav = new ModelAndView();
		Map<String, List<BusLine>> busLines = new HashMap<String, List<BusLine>>();
		List<BusLine> lines = manager.getBusLines();
		for (BusLine line : lines) {
			List<BusStop> stops = new ArrayList<BusStop>();
			for (BusStop stop : line.getStops()) {
				stops.add(stop);
			}
			Collections.sort(stops);
			LinkedHashSet<BusStop> stopsSet = new LinkedHashSet<BusStop>();
			for (BusStop stop : stops) {
				stopsSet.add(stop);
			}
			line.setStops(stopsSet);
		}
		busLines.put("lines", lines);
		mav.addObject("line", busLines);
		Map<String, List<BusStop>> busStops = new HashMap<String, List<BusStop>>();
		busStops.put("stops", manager.getBusStops());
		mav.addObject("stop", busStops);
		if (req.getParameter("line") != null && direct) {
			mav.addObject("lineadd", req.getParameter("line"));
			int lineId = Integer.parseInt(req.getParameter("line"));
			BusLine line = manager.getBusLineById(lineId);
			mav.addObject("linenr", line.getNumber());
			List<BusStop> allStops = manager.getBusStops();
			List<BusStop> stops = new ArrayList<BusStop>();
			boolean isThere = false;
			for (BusStop stop : allStops) {
				for (BusStop stop2 : line.getStops()) {
					if (stop2.getId() == stop.getId()) {
						isThere = true;
					}
				}
				if (isThere == false) {
					stops.add(stop);
				} else {
					isThere = false;
				}
			}
			if (stops.isEmpty()) {
				stops = null;
			}
			busStops.put("stops", stops);
		} else {
			busStops.put("stops", manager.getBusStops());
		}
		mav.addObject("stop", busStops);
		direct = true;
		return mav;
	}

	/**
	 * Meetod manageri saamiseks
	 * 
	 * @return Manager
	 */
	public Manager getManager() {
		return manager;
	}

	/**
	 * Meetod manageri sättimiseks
	 * 
	 * @param manager
	 *            Manager
	 */
	public void setManager(Manager manager) {
		this.manager = manager;
	}

}
