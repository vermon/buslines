package ee.buslines.mvc.controller;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import ee.buslines.BusStop;
import ee.buslines.manager.Manager;

/**
*	Kontroller bussipeatuse muutmisele
*/
public class ChangeBusStopController extends SimpleFormController {
	
	/**
	*	Logija tegevuste logimiseks
	*/
	protected final Logger logger = Logger.getLogger(getClass());
	
	/**
	*	Bussiliinide ja peatuste manager
	*/
	private Manager manager;

	/**
	*	Meetod vormi saatmise korral bussipeatuse objekti muutmiseks ja uue vaate tagastamiseks
	*	@param command Muudetav bussipeatus
	*	@return Järgmine vaade
	*	@throws ServletException
	*/
    public ModelAndView onSubmit(Object command)
            throws ServletException {
    	
        	BusStop busStop = ((BusStop)command);
        	manager.saveBusStop(busStop);
        	logger.info("Bussipeatus nimega "+busStop.getName()+" muudetud");
 
  

        return new ModelAndView(new RedirectView(getSuccessView()));
    }
    
	/**
	*	Meetod vormi algsete väärtuste(algse bussipeatuse) sättimiseks
	*	@param request Liides päringust parameetrite saamiseks
	*	@return Algne bussipeatus
	*	@throws ServletException
	*/
    protected Object formBackingObject(HttpServletRequest request) throws ServletException {
    	
        	BusStop busStop = manager.getBusStopById(Integer.parseInt(request.getParameter("stop")));
            return busStop;
    }
    

	/**
	*	Meetod manageri saamiseks
	*	@return Manager
	*/
	public Manager getManager() {
		return manager;
	}
	
	/**
	*	Meetod manageri sättimiseks
	*	@param manager Manager
	*/
	public void setManager(Manager manager) {
		this.manager = manager;
	}


}
