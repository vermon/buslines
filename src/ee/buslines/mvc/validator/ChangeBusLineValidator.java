package ee.buslines.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ee.buslines.BusLine;
import ee.buslines.manager.Manager;

/**
*	Bussiliini muutmise valideerija
*/
public class ChangeBusLineValidator implements Validator{

	/**
	*	Bussiliinide ja peatuste manager
	*/	
	Manager manager;

	/**
	*	Meetod, mis kontrollib, kas klass on toetatud.
	*	@param arg0 Kontrollitav klass
	*	@return True, kui klassid on võrdsed, False kui ei ole.
	*/
	public boolean supports(Class arg0) {
		return arg0.equals(BusLine.class);
	}
	
	/**
	*	Meetod bussiliini muutmise vormi õigsuse kontrollimiseks
	*	@param obj Vormist saadud objekt
	*	@param errors Vormi töötlemisel tekkinud vead
	*/
	public void validate(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "number", "Sisesta bussiliini number");
		ValidationUtils.rejectIfEmpty(errors, "duration", "Sisesta bussiliini kestvus");
		BusLine busLine = (BusLine) obj;
		if (busLine == null) {
			
			errors.reject("error.nullpointer", "Bussiliini ei õnnestunud muuta");
		}
		if(busLine.getNumber()<0) {
			errors.reject("error.number", "Bussiliini number peab olema positiivne täisarv");
		}
		if(busLine.getDuration()<0) {
			errors.reject("error.duration", "Bussiliini kestvus peab olema positiivne reaalarv");
		}
		for(BusLine line : manager.getBusLines()) {
			if(line.getNumber().equals(busLine.getNumber())) {
				errors.reject("error.exists", "Muudatusi ei ole tehtud või sellise numbriga bussiliin on juba olemas");
				break;
			}
		}
		
	}

	/**
	*	Meetod manageri saamiseks
	*	@return Manager
	*/
	public Manager getManager() {
		return manager;
	}
	
	/**
	*	Meetod manageri sättimiseks
	*	@param manager Manager
	*/
	public void setManager(Manager manager) {
		this.manager = manager;
	}

}
