package ee.buslines.mvc.validator;

import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


import ee.buslines.BusStop;
import ee.buslines.manager.Manager;

/**
*	Bussipeatuse muutmise valideerija
*/
public class ChangeBusStopValidator implements Validator{
	
	/**
	*	Bussiliinide ja peatuste manager
	*/
	Manager manager;

	/**
	*	Meetod, mis kontrollib, kas klass on toetatud.
	*	@param arg0 Kontrollitav klass
	*	@return True, kui klassid on võrdsed, False kui ei ole.
	*/
	public boolean supports(Class arg0) {
		return arg0.equals(BusStop.class);
	}

	/**
	*	Meetod bussipeatuse muutmise vormi õigsuse kontrollimiseks
	*	@param obj Vormist saadud objekt
	*	@param errors Vormi töötlemisel tekkinud vead
	*/
	public void validate(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "name", "Sisesta nimi");
		BusStop busStop = (BusStop) obj;
		if (busStop == null) {
			
			errors.reject("error.nullpointer", "Bussipeatust ei õnnestunud muuta");;
		}
		if(!Pattern.matches("[a-zA-ZöäüõÖÄÜÕ͆]+[ a-zA-ZöäüõÖÄÜÕ͆.-]*[a-zA-ZöäüõÖÄÜÕ͆.]$",busStop.getName()) && errors.getFieldErrors("name").isEmpty()) {
			errors.reject("error.regex","Kasuta peatuse nimes ainult tähti, tühikuid või punkti");
		}
		for(BusStop stop : manager.getBusStops()) {
			if(stop.getName().equalsIgnoreCase(busStop.getName())) {
				errors.reject("error.exists", "Muudatusi ei ole tehtud või peatus on juba olemas");
			}
		}
		
	}

	/**
	*	Meetod manageri saamiseks
	*	@return Manager
	*/
	public Manager getManager() {
		return manager;
	}
	
	/**
	*	Meetod manageri sättimiseks
	*	@param manager Manager
	*/
	public void setManager(Manager manager) {
		this.manager = manager;
	}

}
