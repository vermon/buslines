<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css"/>
		<title>Bussiliinid</title>
	</head>
	<body>
		<div id="wrapper">
			<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
			<%@include file="/WEB-INF/jsp/inc/menu.jsp" %>
			<div id="content" >
				<h3>Liinid</h3>
				<c:choose>
					<c:when test="${not(empty line.lines)}">
						<c:forEach var="busline" items="${line.lines}">
							<TABLE class="liin">
  								<TR">
  									<TD class="number"><c:out value="${busline.number}"/></TD>
  									<TD>Kestvus: <span class="duration"><c:out value="${busline.duration}"/></span> tundi</TD><TD></TD>
 	 							</TR>
  								<TR>
  								</TR>
  								<TR>
  									<TD></TD><TD><span class="label">Peatused</span></TD><TD class="cell"></TD>
  								</TR>
  								<TR>
  								</TR>
  								<c:forEach items="${busline.stops}" var="stops">
	  								<TR>
	  									<TD></TD>
  										<TD><span class="stops"><c:out value="${stops.name}"/></span></TD>
  										<TD class="cell"><a href="<c:url value="buslines.htm?line=${busline.id}&stop=${stops.id}&method=removeStopFromLine"/>">kustuta</a></TD>
									</TR>
  								</c:forEach>
  								<TR>
  									<TD></TD>
  									<TD><a href="<c:url value="addstop.htm?line=${busline.id}&method=show"/>">lisa peatus</a></TD>
  								</TR>
  						
  								<TR>
  									<TD align="left"><a href="<c:url value="buslines.htm?line=${busline.id}&method=deleteLine"/>">kustuta liin</a></TD>
  									<TD></TD>
  									<TD class="cell"><a href="<c:url value="changebusline.htm?line=${busline.id}"/>">muuda liini</a></TD>
  								</TR>
  								<BR>
  							</TABLE>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<span class="error">�htegi liini ei ole veel lisatud</span>
					</c:otherwise>
				</c:choose>
			</div>
			<%@include file="/WEB-INF/jsp/inc/footer.jsp" %>
		</div>
	</body>
</html>
