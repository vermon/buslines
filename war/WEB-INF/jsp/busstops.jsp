<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css"/>
		<title>Bussiliinid</title>
	</head>
	<body>
		<div id="wrapper">
			<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
			<%@include file="/WEB-INF/jsp/inc/menu.jsp" %>
			<div id="content" >
				<h3>Peatused</h3>
				<c:choose>
					<c:when test="${not(empty stop.stops)}">
						<c:forEach items="${stop.stops}" var="busstop">
							<TABLE class="stops">
  								<TR>
  									<TD></TD<TD class="name"><c:out value="${busstop.name}"/></TD><TD></TD>
  								</TR>
  								<TR>
  									<TD align="left"><a href="<c:url value="changebusstop.htm?stop=${busstop.id}"/>">muuda</a></TD>
  									<TD></TD>
  									<TD align="right"><a href="<c:url value="busstops.htm?stop=${busstop.id}&method=deleteStop"/>">kustuta</a></TD>
  								</TR>
  							</TABLE>
  							<BR>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<span class="error">�htegi peatust ei ole veel lisatud</span>
					</c:otherwise>
				</c:choose>
			</div>
			<%@include file="/WEB-INF/jsp/inc/footer.jsp" %>
		</div>
	</body>
</html>

