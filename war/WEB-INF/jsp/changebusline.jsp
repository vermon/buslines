<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css"/>
		<title>Bussiliinid</title>
	</head>
	<body>
		<div id="wrapper">
			<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
			<%@include file="/WEB-INF/jsp/inc/menu.jsp" %>
			<div id="content" >
				<h3>Liinide muutmine</h3>
   				<form class="form" method="post" action="changebusline.htm">
    				Number:
     	 			<spring:bind path="busBean.number">
         				<input type="text" name="number" value="${status.value}" />
      				</spring:bind>
    				Kestvus:
      				<spring:bind path="busBean.duration">
         				<input type="text" name="duration" value="${status.value}" />
      				</spring:bind>
      				<spring:hasBindErrors name="busBean">
         				<p>Sisestamisel tekkis ${errors.errorCount} viga:</p>
         				<ul>
            				<c:forEach var="errMsgObj" items="${errors.allErrors}">
         						<li>
                  					<spring:message code="${errMsgObj.code}" text="${errMsgObj.defaultMessage}"/>
               					</li>
            				</c:forEach>
         				</ul>
      				</spring:hasBindErrors>
      				<input  type="submit" value="Muuda" />
   				</form>
			</div>
			<%@include file="/WEB-INF/jsp/inc/footer.jsp" %>
		</div>
	</body>
</html>