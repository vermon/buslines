<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css"/>
		<title>Bussiliinid</title>
	</head>
	<body>
		<div id="wrapper">
			<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
			<%@include file="/WEB-INF/jsp/inc/menu.jsp" %>
			<div id="content" >
				<h3>Peatuste muutmine</h3>
			   	<form class="form" method="post" action="changebusstop.htm">
					Nimi:
				  	<spring:bind path="busBean.name">
						<input type="text" name="name" value="${status.value}" />
				  	</spring:bind>
				  	<spring:hasBindErrors name="busBean">
						<p>Sisestamisel tekkis ${errors.errorCount} viga:</p>
					 	<ul>
						<c:forEach var="errMsgObj" items="${errors.allErrors}">
							<li>
								<spring:message code="${errMsgObj.code}" text="${errMsgObj.defaultMessage}"/>
							</li>
						</c:forEach>
					 	</ul>
				  	</spring:hasBindErrors>
      				<input type="submit" value="Muuda" />
      			</form>
			</div>
			<%@include file="/WEB-INF/jsp/inc/footer.jsp" %>
		</div>
	</body>
</html>